# TFM_SVCA


Este trabajo se compone de varias tecnologías que se han divido en diferentes carpetas, donde contienen los archivos de configuración necesarios.

Además, los archivos mencionados en la memoria se encuentran en las siguientes rutas:

	-	server.properties: /Kafka/config/
	-	specs_procesado y spec_errores: /Druid/ 
	-	spark-defaults.conf y metrics.conf: /Spark/Configuracion/conf/
	- 	ficheros de las aplicaciones: /Spark/Proyecto/Codigos
	-	jmx_kafka y jmx_spark: /JMX-Exporter/

