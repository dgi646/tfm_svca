package es.filtrado;

import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.streaming.Trigger;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class Main {

	public static void main(String[] args) throws SQLException, TimeoutException, StreamingQueryException {
		// Carga de las tablas necesarias

		// Configuracion spark
		SparkSession spark = SparkSession.builder().appName("Filtrado").config("spark.metrics.namespace", "Filtrado")
				.config("spark.dynamicAllocation.enabled", "true").config("spark.sql.streaming.metricsEnabled", "true")
				.config("spark.dynamicAllocation.minExecutors", "1").config("spark.dynamicAllocation.maxExecutors", "4")
				.config("spark.sql.adaptive.enabled", "true").config("spark.sql.adaptive.skewJoin.enabled", "true")
				.getOrCreate();

		// Solo mostrar a nivel de error
		spark.sparkContext().setLogLevel("ERROR");

		// Lee los datos de Kafka
		Dataset<Row> df = spark.readStream().format("kafka").option("driver", "org.postgresql.Driver")
				.option("kafka.bootstrap.servers", "localhost:9092").option("subscribe", "filtrado_topic").load();

		df = df.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)", "timestamp");

		// Conexión a BBDD
		String url = "jdbc:postgresql://localhost:5432/postgres?currentSchema=public";
		String user = "postgres";
		String password = "postgres";

		// Carga la conexión a la base de datos
		DataFrameReader reader = spark.read().format("jdbc").option("driver", "org.postgresql.Driver")
				.option("url", url).option("user", user).option("password", password)
				.option("driver", "org.postgresql.Driver");

		Dataset<Row> estacionesAytoMadridDB = reader.option("dbtable",
				"(SELECT e.id estacion_id, e.est_estado_id estado FROM estacion e "
						+ " INNER JOIN institucion i ON i.id = e.institucion_id "
						+ " WHERE i.nombre = 'Ayuntamiento de Madrid') AS tmp ")
				.load().cache();

		Dataset<Row> estacionesXEquiposXMagnitudesDB = reader.option("dbtable",
				"(SELECT exm.estacion_id estacion_id, exm.equipos_id  equipo_id ,exm.magnitud_id magnitud_id FROM estacion_x_magnitud exm ) AS tmp ")
				.load().cache();

		// FILTRO DE MAGNITUD
		Dataset<Row> magnitudesFiltroDatosDB = reader.option("dbtable",
				"(select me.estado estado_inferior, me2.estado estado_superior, ml.*  from mag_limite ml "
						+ " left join med_estado me on me.id = med_estado_inf_id "
						+ " left join med_estado me2 on me2.id = med_estado_sup_id "
						+ " where (ml.fecha_baja is null or ml.fecha_baja >= now())) AS tmp ")
				.load().cache();

		// Transformar en un Dataset de Medicion
		StructType schema = DataTypes.createStructType(
				new StructField[] { DataTypes.createStructField("intervalo", DataTypes.IntegerType, true),
						DataTypes.createStructField("estacion", DataTypes.IntegerType, true),
						DataTypes.createStructField("magnitud", DataTypes.IntegerType, true),
						DataTypes.createStructField("equipo", DataTypes.StringType, true),
						DataTypes.createStructField("nivel", DataTypes.IntegerType, true),
						DataTypes.createStructField("fecha", DataTypes.StringType, true),
						DataTypes.createStructField("valor", DataTypes.DoubleType, true),
						DataTypes.createStructField("estado", DataTypes.StringType, true),
						DataTypes.createStructField("indice", DataTypes.IntegerType, true) });

		// Transformar el Dataset
		Dataset<Row> medicionesEntrantes = df.select(functions.from_json(functions.col("value"), schema).as("data"))
				.select("data.*");
		// MEDICIONES AYUNTAMIENTO MADRID
		// Se obtienen las mediciones de Madrid
		Dataset<Row> medicionesAytoMadrid = filtradoMedicionesAytoMadrid(estacionesAytoMadridDB, medicionesEntrantes);

		// Resto de las mediciones
		Dataset<Row> mediciones = obtenerMedicionesNoAytoMadrid(estacionesAytoMadridDB, medicionesEntrantes);

		// COMPROBACIÓN AYTO MADRID
		// Estaciones activas
		Dataset<Row> medicionesEstacionActiva = filtradoMedicionesEstacionActiva(estacionesAytoMadridDB,
				medicionesAytoMadrid);
		// Equipo activo en la estación
		Dataset<Row> medicionesEquipoActivo = filtradoMedicionesEquipoActivo(estacionesXEquiposXMagnitudesDB,
				medicionesEstacionActiva);
		// Validación de que el campo estado existe en BBDD
		Dataset<Row> medicionesAytoMadridFiltrado = filtradoMagnitudEquipoActivo(estacionesXEquiposXMagnitudesDB,
				medicionesEstacionActiva, medicionesEquipoActivo);

		// Filtrado errores
		Dataset<Row> errores = filtradoErrores(estacionesAytoMadridDB, estacionesXEquiposXMagnitudesDB,
				medicionesAytoMadrid, medicionesEstacionActiva, medicionesEquipoActivo);

		// Agrupación de las mediciones del Ayuntamiento de Madrid y el resto
		mediciones = mediciones.union(medicionesAytoMadridFiltrado);

		// Cambio de estado de las mediciones erróneas y union con el resto de las
		// mediciones
		mediciones = mediciones.union(errores.withColumn("estado", functions.lit("3")).drop(errores.col("error")))
				.select(mediciones.col("*"));

		// Filtrado por el límite de magnitudes
		Dataset<Row> medicionesMagnitudes = medicionesFiltroLimite(magnitudesFiltroDatosDB, medicionesEntrantes);

		// Salida de los datos al topico
		Dataset<Row> salidaDataset = medicionesMagnitudes.toDF();
		salidaDataset = salidaDataset.selectExpr("CAST(null AS STRING) AS key", "to_json(struct(*)) AS value");
		errores = errores.selectExpr("CAST(null AS STRING) AS key", "to_json(struct(*)) AS value");

		StreamingQuery salidaRecalculoTopico = salidaDataset.writeStream().format("kafka")
				.option("kafka.bootstrap.servers", "localhost:9092").trigger(Trigger.ProcessingTime("1 seconds")) // segundos
				.option("checkpointLocation", "/tmp/filtrado/checkpoint").option("topic", "recalculo_topic").start();

		StreamingQuery salidaErroresTopico = errores.writeStream().format("kafka")
				.option("kafka.bootstrap.servers", "localhost:9092").trigger(Trigger.ProcessingTime("1 seconds")) // segundos
				.option("checkpointLocation", "/tmp/filtrado_errores/checkpoint").option("topic", "errores_topic")
				.start();

		unpersistBBDD(estacionesAytoMadridDB, estacionesXEquiposXMagnitudesDB, magnitudesFiltroDatosDB);
		salidaRecalculoTopico.awaitTermination();
		salidaErroresTopico.awaitTermination();
	}

	private static Dataset<Row> filtradoErrores(Dataset<Row> estacionesAytoMadridDB,
			Dataset<Row> estacionesXEquiposXMagnitudesDB, Dataset<Row> medicionesAytoMadrid,
			Dataset<Row> medicionesEstacionActiva, Dataset<Row> medicionesEquipoActivo) {
		// Alerta de cada una de las mediciones que tienen una estación inactiva
		Dataset<Row> medicionesEstacionInactiva = filtradoMedicionesEstacionInactiva(estacionesAytoMadridDB,
				medicionesAytoMadrid);

		// Alerta de cada una de las mediciones que tienen un equipo inactivo
		Dataset<Row> medicionesEquipoInactivo = filtradoMedicionesEquipoInactivo(estacionesXEquiposXMagnitudesDB,
				medicionesEstacionActiva);

		// Alerta de cada una de las mediciones que tienen una magnitud inactiva
		Dataset<Row> medicionesMagnitudInactiva = filtradoAytoMagnitudEquipoInactivo(estacionesXEquiposXMagnitudesDB,
				medicionesEstacionActiva, medicionesEquipoActivo);

		// Errores
		Dataset<Row> errores = medicionesEstacionInactiva.union(medicionesEquipoInactivo)
				.union(medicionesMagnitudInactiva);
		return errores;
	}

	private static Dataset<Row> filtradoAytoMagnitudEquipoInactivo(Dataset<Row> estacionesXEquiposXMagnitudesDB,
			Dataset<Row> medicionesEstacionActiva, Dataset<Row> medicionesEquipoActivo) {
		Dataset<Row> medicionesMagnitudInactiva = medicionesEquipoActivo
				.join(estacionesXEquiposXMagnitudesDB,
						medicionesEquipoActivo.col("magnitud")
								.equalTo(estacionesXEquiposXMagnitudesDB.col("magnitud_id"))
								.and(medicionesEquipoActivo.col("equipo")
										.equalTo(estacionesXEquiposXMagnitudesDB.col("equipo_id"))),
						"left_anti")
				.select(medicionesEstacionActiva.col("*"))
				.withColumn("error", functions.lit("La relación entre la magnitud y el equipo se encuentra inactiva"));
		return medicionesMagnitudInactiva;
	}

	private static Dataset<Row> filtradoMagnitudEquipoActivo(Dataset<Row> estacionesXEquiposXMagnitudesDB,
			Dataset<Row> medicionesEstacionActiva, Dataset<Row> medicionesEquipoActivo) {
		Dataset<Row> medicionesAytoMadridFiltrado = medicionesEquipoActivo
				.join(estacionesXEquiposXMagnitudesDB,
						medicionesEquipoActivo.col("magnitud")
								.equalTo(estacionesXEquiposXMagnitudesDB.col("magnitud_id"))
								.and(medicionesEquipoActivo.col("equipo")
										.equalTo(estacionesXEquiposXMagnitudesDB.col("equipo_id"))),
						"inner")
				.select(medicionesEstacionActiva.col("*"));
		return medicionesAytoMadridFiltrado;
	}

	private static Dataset<Row> filtradoMedicionesEquipoInactivo(Dataset<Row> estacionesXEquiposXMagnitudesDB,
			Dataset<Row> medicionesEstacionActiva) {
		Dataset<Row> medicionesEquipoInactivo = medicionesEstacionActiva
				.join(estacionesXEquiposXMagnitudesDB,
						medicionesEstacionActiva.col("estacion")
								.equalTo(estacionesXEquiposXMagnitudesDB.col("estacion_id"))
								.and(medicionesEstacionActiva.col("equipo")
										.equalTo(estacionesXEquiposXMagnitudesDB.col("equipo_id"))),
						"left_anti")
				.select(medicionesEstacionActiva.col("*"))
				.withColumn("error", functions.lit("La relación entre la estacion y el equipo se encuentra inactiva"));
		return medicionesEquipoInactivo;
	}

	private static Dataset<Row> filtradoMedicionesEquipoActivo(Dataset<Row> estacionesXEquiposXMagnitudesDB,
			Dataset<Row> medicionesEstacionActiva) {
		Dataset<Row> medicionesEquipoActivo = medicionesEstacionActiva
				.join(estacionesXEquiposXMagnitudesDB,
						medicionesEstacionActiva.col("estacion")
								.equalTo(estacionesXEquiposXMagnitudesDB.col("estacion_id"))
								.and(medicionesEstacionActiva.col("equipo")
										.equalTo(estacionesXEquiposXMagnitudesDB.col("equipo_id"))),
						"inner")
				.select(medicionesEstacionActiva.col("*"));
		return medicionesEquipoActivo;
	}

	private static Dataset<Row> filtradoMedicionesEstacionInactiva(Dataset<Row> estacionesAytoMadridDB,
			Dataset<Row> medicionesAytoMadrid) {
		Dataset<Row> medicionesEstacionInactiva = medicionesAytoMadrid.join(estacionesAytoMadridDB.filter("estado = 5"),
				medicionesAytoMadrid.col("estacion").equalTo(estacionesAytoMadridDB.col("estacion_id")), "left_anti")
				.select(medicionesAytoMadrid.col("*"))
				.withColumn("error", functions.lit("La estacion se encuentra inactiva"));
		return medicionesEstacionInactiva;
	}

	private static Dataset<Row> filtradoMedicionesEstacionActiva(Dataset<Row> estacionesAytoMadridDB,
			Dataset<Row> medicionesAytoMadrid) {
		Dataset<Row> medicionesEstacionActiva = medicionesAytoMadrid.join(estacionesAytoMadridDB.filter("estado = 5"),
				medicionesAytoMadrid.col("estacion").equalTo(estacionesAytoMadridDB.col("estacion_id")), "inner")
				.select(medicionesAytoMadrid.col("*"));
		return medicionesEstacionActiva;
	}

	private static Dataset<Row> obtenerMedicionesNoAytoMadrid(Dataset<Row> estacionesAytoMadridDB,
			Dataset<Row> medicionesEntrantes) {
		Dataset<Row> mediciones = medicionesEntrantes.join(estacionesAytoMadridDB,
				medicionesEntrantes.col("estacion").equalTo(estacionesAytoMadridDB.col("estacion_id")), "left_anti")
				.select(medicionesEntrantes.col("*"));
		return mediciones;
	}

	private static Dataset<Row> filtradoMedicionesAytoMadrid(Dataset<Row> estacionesAytoMadridDB,
			Dataset<Row> medicionesEntrantes) {
		Dataset<Row> medicionesAytoMadrid = medicionesEntrantes
				.join(estacionesAytoMadridDB,
						medicionesEntrantes.col("estacion").equalTo(estacionesAytoMadridDB.col("estacion_id")), "inner")
				.select(medicionesEntrantes.col("*"));
		return medicionesAytoMadrid;
	}

	private static Dataset<Row> medicionesFiltroLimite(Dataset<Row> magnitudesFiltroDatosDB,
			Dataset<Row> medicionesEntrantes) {
		Dataset<Row> medicionesMagnitudesFiltrar = medicionesEntrantes.join(magnitudesFiltroDatosDB,
				medicionesEntrantes.col("magnitud").equalTo(magnitudesFiltroDatosDB.col("magnitud_id")), "inner");

		Dataset<Row> medicionesMagnitudesNoFiltrar = medicionesEntrantes.join(magnitudesFiltroDatosDB,
				medicionesEntrantes.col("magnitud").equalTo(magnitudesFiltroDatosDB.col("magnitud_id")), "left_anti");

		// Modificación del estado y valor en función de los límites
		medicionesMagnitudesFiltrar = filtradoLimites(medicionesMagnitudesFiltrar);

		// Obtenemos solamente aquellos campos que se necesitan
		Dataset<Row> salidaMedicionesFiltradas = medicionesMagnitudesFiltrar.select("intervalo", "estacion", "magnitud",
				"equipo", "nivel", "fecha", "valor", "estado", "indice");

		Dataset<Row> medicionesMagnitudes = salidaMedicionesFiltradas.union(medicionesMagnitudesNoFiltrar);
		return medicionesMagnitudes;
	}

	private static Dataset<Row> filtradoLimites(Dataset<Row> medicionesMagnitudesFiltrar) {
		medicionesMagnitudesFiltrar = medicionesMagnitudesFiltrar
				.withColumn("estado",
						functions
								.when(medicionesMagnitudesFiltrar.col("limite_inferior").isNotNull()
										.and(medicionesMagnitudesFiltrar.col("valor")
												.$less(medicionesMagnitudesFiltrar.col("limite_inferior"))),
										functions
												.when(medicionesMagnitudesFiltrar.col("mantener_est_inf"),
														medicionesMagnitudesFiltrar.col("estado"))
												.otherwise(medicionesMagnitudesFiltrar.col("estado_inferior")))
								.when(medicionesMagnitudesFiltrar.col("limite_superior").isNotNull()
										.and(medicionesMagnitudesFiltrar.col("valor")
												.$greater(medicionesMagnitudesFiltrar.col("limite_superior"))),
										functions
												.when(medicionesMagnitudesFiltrar.col("mantener_est_sup"),
														medicionesMagnitudesFiltrar.col("estado"))
												.otherwise(medicionesMagnitudesFiltrar.col("estado_superior")))
								.otherwise(medicionesMagnitudesFiltrar.col("estado")))
				.withColumn("valor",
						functions
								.when(medicionesMagnitudesFiltrar.col("limite_inferior").isNotNull()
										.and(medicionesMagnitudesFiltrar.col("valor")
												.$less(medicionesMagnitudesFiltrar.col("limite_inferior"))),
										functions
												.when(medicionesMagnitudesFiltrar.col("mantener_valor_inf"),
														medicionesMagnitudesFiltrar.col("valor"))
												.otherwise(medicionesMagnitudesFiltrar.col("nuevo_valor_inf")))
								.when(medicionesMagnitudesFiltrar.col("limite_superior").isNotNull()
										.and(medicionesMagnitudesFiltrar.col("valor")
												.$greater(medicionesMagnitudesFiltrar.col("limite_superior"))),
										functions
												.when(medicionesMagnitudesFiltrar.col("mantener_valor_sup"),
														medicionesMagnitudesFiltrar.col("valor"))
												.otherwise(medicionesMagnitudesFiltrar.col("nuevo_valor_sup")))
								.otherwise(medicionesMagnitudesFiltrar.col("valor")));
		return medicionesMagnitudesFiltrar;
	}

	private static void unpersistBBDD(Dataset<Row> estacionesAytoMadridDB, Dataset<Row> estacionesXEquiposXMagnitudesDB,
			Dataset<Row> magnitudesFiltroDatosDB) {
		estacionesAytoMadridDB.unpersist();
		estacionesXEquiposXMagnitudesDB.unpersist();
		magnitudesFiltroDatosDB.unpersist();
	}

}
