package es.validacion;

import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

import org.apache.spark.sql.DataFrameReader;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.streaming.Trigger;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

public class Main {

	public static void main(String[] args) throws SQLException, TimeoutException, StreamingQueryException {
		// Configuracion spark
		SparkSession spark = SparkSession.builder().appName("Validacion").config("spark.executor.cores", "1")
				.config("spark.dynamicAllocation.minExecutors", "1").config("spark.dynamicAllocation.maxExecutors", "4")
				.config("spark.metrics.namespace", "Validacion").config("spark.sql.streaming.metricsEnabled", "true")
				.config("spark.sql.adaptive.enabled", "true").config("spark.sql.adaptive.skewJoin.enabled", "true")
				.getOrCreate();

		// Solo mostrar a nivel de error
		spark.sparkContext().setLogLevel("ERROR");

		// Lee los datos de Kafka
		Dataset<Row> sparkDF = spark.readStream().format("kafka").option("kafka.bootstrap.servers", "localhost:9092")
				.option("subscribe", "entrada_topic").option("startingOffsets", "latest").load();
		Dataset<Row> df = sparkDF.selectExpr("CAST(key AS STRING) as key", "CAST(value AS STRING) as values");

		// Transformar en un Dataset de Medicion
		StructType schema = DataTypes.createStructType(
				new StructField[] { DataTypes.createStructField("fuente_datos", DataTypes.StringType, false),
						DataTypes.createStructField("intervalo", DataTypes.IntegerType, true),
						DataTypes.createStructField("estacion", DataTypes.IntegerType, true),
						DataTypes.createStructField("magnitud", DataTypes.IntegerType, true),
						DataTypes.createStructField("equipo", DataTypes.StringType, true),
						DataTypes.createStructField("nivel", DataTypes.IntegerType, true),
						DataTypes.createStructField("fecha", DataTypes.TimestampType, true),
						DataTypes.createStructField("valor", DataTypes.DoubleType, true),
						DataTypes.createStructField("estado", DataTypes.StringType, true),
						DataTypes.createStructField("indice", DataTypes.IntegerType, true) });

		df = df.select(functions.from_json(functions.col("values"), schema).as("data")).select("data.*");

		// Conexión a BBDD
		String url = "jdbc:postgresql://localhost:5432/postgres?currentSchema=public";
		String user = "postgres";
		String password = "postgres";

		// Carga la conexión a la base de datos
		DataFrameReader reader = spark.read().format("jdbc").option("driver", "org.postgresql.Driver")
				.option("url", url).option("user", user).option("password", password);

		// Carga de las tablas necesarias
		Dataset<Row> fuentesDatosDB = reader.option("dbtable", "est_fuente_datos").load().select("codigo").cache();
		Dataset<Row> equiposDB = reader.option("dbtable", "equipo").load().select("id", "codigo", "eq_tipologia_id")
				.cache();
		Dataset<Row> estacionesDB = reader.option("dbtable", "estacion").load().select("id", "codigo").cache();
		Dataset<Row> magnitudesDB = reader.option("dbtable", "magnitud").load().select("id", "codigo").cache();
		Dataset<Row> estadosMedicionDB = reader.option("dbtable", "med_estado").load().select("id", "estado").cache();
		Dataset<Row> intervalosDB = reader.option("dbtable", "intervalo_integracion").load().select("intervalo")
				.cache();

		// VALIDACION
		Dataset<Row> mediciones = validarElementos(df, fuentesDatosDB, equiposDB, estacionesDB, magnitudesDB,
				estadosMedicionDB, intervalosDB);

		// ERRORES
		Dataset<Row> errores = extraerErrores(df, fuentesDatosDB, equiposDB, estacionesDB, magnitudesDB,
				estadosMedicionDB, intervalosDB);

		errores = errores.selectExpr("CAST(null AS STRING) AS key", "to_json(struct(*)) AS value");

		Dataset<Row> salidaDatasetTopico = mediciones
				.selectExpr("intervalo", "estacion_id as estacion", "magnitud_id as magnitud", "equipo_id as equipo",
						"nivel", "fecha", "valor", "med_estado_id as estado", "indice")
				.selectExpr("CAST(null AS STRING) AS key", "to_json(struct(*)) AS value");

		StreamingQuery salidaFiltradoTopico = salidaDatasetTopico.writeStream().format("kafka")
				.option("kafka.bootstrap.servers", "localhost:9092")
				.option("checkpointLocation", "/tmp/validacion/checkpoint").option("topic", "filtrado_topic")
				.trigger(Trigger.ProcessingTime("1 seconds")).start();

		StreamingQuery salidaErroresTopico = errores.writeStream().format("kafka")
				.option("kafka.bootstrap.servers", "localhost:9092")
				.option("checkpointLocation", "/tmp/validacion_errores/checkpoint").option("topic", "errores_topic")
				.trigger(Trigger.ProcessingTime("1 seconds")).start();

		unpersistBBDD(fuentesDatosDB, equiposDB, estacionesDB, magnitudesDB, estadosMedicionDB, intervalosDB);

		salidaFiltradoTopico.awaitTermination();
		salidaErroresTopico.awaitTermination();

	}

	private static void unpersistBBDD(Dataset<Row> fuentesDatosDB, Dataset<Row> equiposDB, Dataset<Row> estacionesDB,
			Dataset<Row> magnitudesDB, Dataset<Row> estadosMedicionDB, Dataset<Row> intervalosDB) {
		equiposDB.unpersist();
		estacionesDB.unpersist();
		magnitudesDB.unpersist();
		intervalosDB.unpersist();
		estadosMedicionDB.unpersist();
		fuentesDatosDB.unpersist();
	}

	private static Dataset<Row> extraerErrores(Dataset<Row> df, Dataset<Row> fuentesDatosDB, Dataset<Row> equiposDB,
			Dataset<Row> estacionesDB, Dataset<Row> magnitudesDB, Dataset<Row> estadosMedicionDB,
			Dataset<Row> intervalosDB) {
		// Mediciones que no superan la validación de fuentes de datos
		Dataset<Row> errores = erroresFuenteDatos(df, fuentesDatosDB);
		// Mediciones que no superan la validación de Sodar
		errores = erroresSodar(df, errores);
		// Mediciones que no superan la validación de Meteo
		errores = erroresMeteo(df, equiposDB, errores);
		// Mediciones que no superan la validación de equipo
		errores = erroresEquipo(df, equiposDB, errores);
		// Mediciones que no superan la validación de estación
		errores = erroresEstacion(df, estacionesDB, errores);
		// Mediciones que no superan la validación de magnitud
		errores = erroresMagnitud(df, magnitudesDB, errores);
		// Mediciones que no superan la validación de estado
		errores = erroresEstado(df, estadosMedicionDB, errores);
		// Mediciones que no superan la validación de intervalo
		errores = erroresIntervalo(df, intervalosDB, errores);
		return errores;
	}

	private static Dataset<Row> validarElementos(Dataset<Row> df, Dataset<Row> fuentesDatosDB, Dataset<Row> equiposDB,
			Dataset<Row> estacionesDB, Dataset<Row> magnitudesDB, Dataset<Row> estadosMedicionDB,
			Dataset<Row> intervalosDB) {
		// Validación de que el campo fuente de datos existe en BBDD
		Dataset<Row> mediciones = validarFuenteDatos(df, fuentesDatosDB);
		// Validación de que la fuente de datos Sodar tiene nivel
		mediciones = validarSodar(mediciones);
		// Validación de que la fuente de datos Meteo el equipo asociado su tipo es
		// meteorología
		mediciones = validarMeteo(equiposDB, mediciones);
		// Validación de que el campo equipo existe en BBDD
		mediciones = validarEquipo(equiposDB, mediciones);
		// Validación de que el campo estacion existe en BBDD
		mediciones = validarEstacion(estacionesDB, mediciones);
		// Validación de que el campo magnitud existe en BBDD
		mediciones = validarMagnitud(magnitudesDB, mediciones);
		// Validación de que el campo estado existe en BBDD
		mediciones = validarEstado(estadosMedicionDB, mediciones);
		// Validación de que el campo estado existe en BBDD
		mediciones = validarIntervalo(intervalosDB, mediciones);
		return mediciones;
	}

	private static Dataset<Row> erroresMeteo(Dataset<Row> df, Dataset<Row> equiposDB, Dataset<Row> errores) {
		errores = df.filter(functions.col("fuente_datos").equalTo("28079045"))
				.join(equiposDB, equiposDB.col("codigo").equalTo(df.col("equipo")), "inner")
				.filter(equiposDB.col("eq_tipologia_id").notEqual("2")).select(df.col("*"))
				.withColumn("error",
						functions.lit("El equipo asociado es de tipo meteorología con la fuente de datos Meteo"))
				.union(errores);
		return errores;
	}

	private static Dataset<Row> validarMeteo(Dataset<Row> equiposDB, Dataset<Row> mediciones) {
		Dataset<Row> medicionesMeteo = mediciones.filter(functions.col("fuente_datos").equalTo("28079045"))
				.join(equiposDB, equiposDB.col("codigo").equalTo(mediciones.col("equipo")), "inner")
				.filter(equiposDB.col("eq_tipologia_id").equalTo("2")).select(mediciones.col("*"));
		mediciones = mediciones.filter(functions.col("fuente_datos").notEqual("28079045")).select(mediciones.col("*"));
		mediciones = mediciones.union(medicionesMeteo);
		return mediciones;
	}

	private static Dataset<Row> erroresSodar(Dataset<Row> df, Dataset<Row> errores) {
		errores = df.filter(functions.col("fuente_datos").equalTo("28079095").and(functions.col("nivel").isNull()))
				.select(df.col("*")).withColumn("error", functions.lit("La fuente de datos SODAR no tiene nivel"))
				.union(errores);
		return errores;
	}

	private static Dataset<Row> erroresFuenteDatos(Dataset<Row> df, Dataset<Row> fuentesDatosDB) {
		Dataset<Row> errores = df
				.join(fuentesDatosDB, df.col("fuente_datos").equalTo(fuentesDatosDB.col("codigo")), "left_anti")
				.withColumn("error", functions.lit("La fuente de datos no existe"));
		return errores;
	}

	private static Dataset<Row> validarFuenteDatos(Dataset<Row> df, Dataset<Row> fuentesDatosDB) {
		Dataset<Row> mediciones = df
				.join(fuentesDatosDB, df.col("fuente_datos").equalTo(fuentesDatosDB.col("codigo")), "inner")
				.withColumn("tiempo_fValido", functions.current_timestamp());
		return mediciones;
	}

	private static Dataset<Row> validarSodar(Dataset<Row> mediciones) {
		mediciones = mediciones
				.filter(functions.col("fuente_datos").equalTo("28079095").and(functions.col("nivel").isNotNull())
						.or(functions.col("fuente_datos").notEqual("28079095")))
				.select(mediciones.col("*")).withColumn("tiempo_fSodar", functions.current_timestamp());
		return mediciones;
	}

	private static Dataset<Row> erroresIntervalo(Dataset<Row> df, Dataset<Row> intervalosDB, Dataset<Row> errores) {
		errores = df.join(intervalosDB, df.col("intervalo").equalTo(intervalosDB.col("intervalo")), "left_anti")
				.select(df.col("*")).withColumn("error", functions.lit("El intervalo no existe")).union(errores);
		return errores;
	}

	private static Dataset<Row> validarIntervalo(Dataset<Row> intervalosDB, Dataset<Row> mediciones) {
		mediciones = mediciones
				.join(intervalosDB, mediciones.col("intervalo").equalTo(intervalosDB.col("intervalo")), "inner")
				.select(mediciones.col("*"));
		return mediciones;
	}

	private static Dataset<Row> erroresEstado(Dataset<Row> df, Dataset<Row> estadosMedicionDB, Dataset<Row> errores) {
		errores = df.join(estadosMedicionDB, df.col("estado").equalTo(estadosMedicionDB.col("estado")), "left_anti")
				.select(df.col("*")).withColumn("error", functions.lit("El estado no existe")).union(errores);
		return errores;
	}

	private static Dataset<Row> validarEstado(Dataset<Row> estadosMedicionDB, Dataset<Row> mediciones) {
		mediciones = mediciones
				.join(estadosMedicionDB, mediciones.col("estado").equalTo(estadosMedicionDB.col("estado")), "inner")
				.select(mediciones.col("*"),
						estadosMedicionDB.col("id").cast(DataTypes.IntegerType).as("med_estado_id"));
		return mediciones;
	}

	private static Dataset<Row> erroresMagnitud(Dataset<Row> df, Dataset<Row> magnitudesDB, Dataset<Row> errores) {
		errores = df.join(magnitudesDB, df.col("magnitud").equalTo(magnitudesDB.col("codigo")), "left_anti")
				.select(df.col("*")).withColumn("error", functions.lit("La magnitud no existe")).union(errores);
		return errores;
	}

	private static Dataset<Row> validarMagnitud(Dataset<Row> magnitudesDB, Dataset<Row> mediciones) {
		mediciones = mediciones
				.join(magnitudesDB, mediciones.col("magnitud").equalTo(magnitudesDB.col("codigo")), "inner")
				.select(mediciones.col("*"), magnitudesDB.col("id").as("magnitud_id"));
		return mediciones;
	}

	private static Dataset<Row> erroresEstacion(Dataset<Row> df, Dataset<Row> estacionesDB, Dataset<Row> errores) {
		errores = df.join(estacionesDB, df.col("estacion").equalTo(estacionesDB.col("codigo")), "left_anti")
				.select(df.col("*")).withColumn("error", functions.lit("La estacion no existe")).union(errores);
		return errores;
	}

	private static Dataset<Row> validarEstacion(Dataset<Row> estacionesDB, Dataset<Row> mediciones) {
		mediciones = mediciones
				.join(estacionesDB, mediciones.col("estacion").equalTo(estacionesDB.col("codigo")), "inner")
				.select(mediciones.col("*"), estacionesDB.col("id").cast(DataTypes.IntegerType).as("estacion_id"));
		return mediciones;
	}

	private static Dataset<Row> validarEquipo(Dataset<Row> equiposDB, Dataset<Row> mediciones) {
		mediciones = mediciones.join(equiposDB, mediciones.col("equipo").equalTo(equiposDB.col("codigo")), "inner")
				.select(mediciones.col("*"), equiposDB.col("id").as("equipo_id"))
				.withColumn("tiempo_equipo", functions.current_timestamp());
		return mediciones;
	}

	private static Dataset<Row> erroresEquipo(Dataset<Row> df, Dataset<Row> equiposDB, Dataset<Row> errores) {
		errores = df.join(equiposDB, df.col("equipo").equalTo(equiposDB.col("codigo")), "left_anti")
				.withColumn("error", functions.lit("El equipo no existe")).union(errores);
		return errores;
	}

}
