package es.recalculo;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.sivca.dominio.Medicion;

public class Main {

	private static final String magnitudNO = "7";
	private static final String magnitudNO2 = "8";
	private static final String magnitudTOL = "220";
	private static final String magnitudBEN = "71";
	private static final String magnitudEBE = "166";
	private static final String magnitudHCT = "37";
	private static final String magnitudCH4 = "38";

	private static final String APP_NAME = "Recalculo";

	public static void main(String[] args) throws StreamingQueryException, InterruptedException, SQLException {

		// Configuración de Spark
		SparkConf conf = new SparkConf().set("spark.sql.adaptive.enabled", "true").set("spark.executor.cores", "1")
				.set("spark.dynamicAllocation.minExecutors", "1").set("spark.dynamicAllocation.maxExecutors", "4")
				.set("spark.metrics.namespace", "Recalculo").set("spark.sql.streaming.metricsEnabled", "true")
				.set("spark.dynamicAllocation.enabled", "true").set("spark.sql.adaptive.skewJoin.enabled", "true")
				.setAppName(APP_NAME);

		JavaStreamingContext ssc = new JavaStreamingContext(conf, new Duration(1000));
		SparkSession spark = SparkSession.builder().config(conf).getOrCreate();// Initialize KafkaConsumer
		spark.sparkContext().setLogLevel("ERROR");

		// Parametros del servidor
		Map<String, Object> kafkaParams = new HashMap<String, Object>();
		String kafkaServer = "localhost:9092";
		kafkaParams.put("bootstrap.servers", kafkaServer);
		kafkaParams.put("key.deserializer", StringDeserializer.class);
		kafkaParams.put("value.deserializer", StringDeserializer.class);
		kafkaParams.put("group.id", "1");
		kafkaParams.put("auto.offset.reset", "latest");
		kafkaParams.put("enable.auto.commit", false);

		// Parametros deserializador
		Map<String, Object> kafkaParamsOut = new HashMap<String, Object>();
		String kafkaServerOut = "localhost:9092";
		kafkaParamsOut.put("bootstrap.servers", kafkaServerOut);
		kafkaParamsOut.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		kafkaParamsOut.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

		String topic = "recalculo_topic";
		Collection<String> topics = Arrays.asList(topic);

		// Stream que recibe los datos en crudo
		JavaInputDStream<ConsumerRecord<String, String>> stream = KafkaUtils.createDirectStream(ssc,
				LocationStrategies.PreferConsistent(),
				ConsumerStrategies.<String, String>Subscribe(topics, kafkaParams));

		// Se transforman a la clase medición
		JavaDStream<Medicion> userEvents = stream.map(record -> {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(record.value(), Medicion.class);
		});

		// Se convierte en Dataset y se hace una transformación simple
		userEvents.foreachRDD(rdd -> {

			if (!rdd.isEmpty()) {

				Dataset<Row> medicionesEntrantes = spark.createDataset(rdd.rdd(), Encoders.bean(Medicion.class)).toDF()
						.cache();
				Dataset<Row> medFiltradas = filtrarOxidos(medicionesEntrantes);
				medFiltradas = filtrarBTX(medicionesEntrantes);
				medFiltradas = filtrarHCT(medicionesEntrantes);

				// Mediciones no correctas
				Dataset<Medicion> medicionesNoCorrectas = medicionesEntrantes.join(medFiltradas,
						medicionesEntrantes.col("estacion").equalTo(medFiltradas.col("estacion"))
								.and(medicionesEntrantes.col("fecha").equalTo(medFiltradas.col("fecha"))),
						"left_anti").withColumn("estado", functions.lit("3")).as(Encoders.bean(Medicion.class));
				// Union de las mediciones
				medFiltradas = medFiltradas.withColumn("magnitud", functions.col("magnitud").cast("INT"));
				medFiltradas = medFiltradas.unionAll(medicionesNoCorrectas.toDF());

				// Envia las mediciones al topico de errores
				JavaRDD<Row> med_errores = medicionesNoCorrectas.toDF().toJavaRDD();
				med_errores.foreachPartition(records -> {
					Producer<String, String> kafkaProducer = new KafkaProducer<>(kafkaParamsOut);

					while (records.hasNext()) {
						Row medicion = records.next();
						// Convertir el objeto Medicion a JSON
						String json = medicion.json();
						// Enviar el registro a Kafka
						ProducerRecord<String, String> record = new ProducerRecord<>("errores_topic", json);
						kafkaProducer.send(record);
					}
					kafkaProducer.close();
				});
				// Envia las mediciones al topico de agregacion
				JavaRDD<Row> med = medFiltradas.toJavaRDD();
				med.foreachPartition(records -> {
					Producer<String, String> kafkaProducer = new KafkaProducer<>(kafkaParamsOut);

					while (records.hasNext()) {
						Row medicion = records.next();
						// Convertir el objeto Medicion a JSON
						String json = medicion.json();
						// Enviar el registro a Kafka
						ProducerRecord<String, String> record = new ProducerRecord<>("agregacion_topic", json);
						kafkaProducer.send(record);
					}
					kafkaProducer.close();
				});
				medicionesEntrantes.unpersist();

			}
		});

		ssc.start();
		ssc.awaitTermination();
	}

	public static Dataset<Row> filtrarOxidos(Dataset<Row> medicionesEntrantes) {

		return medicionesEntrantes;
	}

	public static Dataset<Row> filtrarBTX(Dataset<Row> medicionesEntrantes) {

		Dataset<Row> medicionesFiltradas = filtradoBTX(medicionesEntrantes);
		Dataset<Row> medicionesRecalculadas = convertirRowAMedicion(medicionesFiltradas);

		return medicionesRecalculadas;
	}

	private static Dataset<Row> convertirRowAMedicion(Dataset<Row> medicionesFiltradas) {
		return medicionesFiltradas.select("equipo", "estacion", "estado", "fecha", "indice", "intervalo", "magnitud",
				"nivel", "valor");
	}

	public static Dataset<Row> filtrarHCT(Dataset<Row> medicionesEntrantes) {

		Dataset<Row> medicionesFiltradas = filtradoHCT(medicionesEntrantes);
		Dataset<Row> medicionesRecalculadas = recalculoHCT(medicionesFiltradas);

		return medicionesRecalculadas;
	}

	public static Dataset<Row> recalculoOxidos(Dataset<Row> medicionesFiltradas) {
		// Se realiza el recálculo de NOx
		Dataset<Row> medicionesRecalculoNOx = medicionesFiltradas.filter(medicionesFiltradas.col("magnitud")
				.isin(magnitudNO, magnitudNO2).and(medicionesFiltradas.col("estado").notEqual(3)))
				.groupBy("estacion", "fecha")
				.agg(functions.max(functions.when(functions.col("magnitud").equalTo(magnitudNO), functions.col("valor"))
						.otherwise(null)).as("valor_NO"),
						functions.max(
								functions.when(functions.col("magnitud").equalTo(magnitudNO2), functions.col("valor"))
										.otherwise(null))
								.as("valor_NO2"))
				.filter(functions.col("valor_NO").isNotNull().and(functions.col("valor_NO2").isNotNull()))
				.withColumn("valor_recalculado",
						functions.col("valor_NO").multiply(1.5333).$plus(functions.col("valor_NO2")))
				.withColumnRenamed("fecha", "fecha_recalculo").withColumnRenamed("estacion", "estacion_recalculo");
		;
		return unionRecalculo(medicionesFiltradas, medicionesRecalculoNOx);
	}

	public static Dataset<Row> filtradoBTX(Dataset<Row> medicionesEntrantes) {
		Dataset<Row> medicionesErroneasNONO2 = medicionesEntrantes
				.filter(medicionesEntrantes.col("magnitud").isin(magnitudTOL, magnitudBEN, magnitudEBE))
				.groupBy("estacion", "fecha")
				.agg(functions.max(functions
						.when(functions.col("magnitud").equalTo(magnitudTOL), functions.col("valor")).otherwise(null))
						.as("magnitud_TOL"),
						functions.max(
								functions.when(functions.col("magnitud").equalTo(magnitudBEN), functions.col("valor"))
										.otherwise(null))
								.as("magnitud_BEN"),
						functions.max(
								functions.when(functions.col("magnitud").equalTo(magnitudEBE), functions.col("valor"))
										.otherwise(null))
								.as("magnitud_EBE"))
				.withColumn("test",
						functions.when(functions.col("magnitud_TOL").isNull().or(functions.col("magnitud_BEN").isNull())
								.or(functions.col("magnitud_EBE").isNull()), 0).otherwise(1))
				.filter(functions.col("test").equalTo(0));

		return unionFiltrado(medicionesEntrantes, medicionesErroneasNONO2);
	}

	public static Dataset<Row> filtradoHCT(Dataset<Row> medicionesEntrantes) {
		Dataset<Row> medicionesErroneasNONO2 = medicionesEntrantes
				.filter(medicionesEntrantes.col("magnitud").isin(magnitudHCT, magnitudCH4)).groupBy("estacion", "fecha")
				.agg(functions.max(functions
						.when(functions.col("magnitud").equalTo(magnitudHCT), functions.col("valor")).otherwise(null))
						.as("magnitud_HCT"),
						functions.max(
								functions.when(functions.col("magnitud").equalTo(magnitudCH4), functions.col("valor"))
										.otherwise(null))
								.as("magnitud_CH4"))
				.withColumn("test",
						functions.when(
								functions.col("magnitud_HCT").isNull()
										.or(functions.col("magnitud_CH4").isNull().or(
												functions.col("magnitud_CH4").$less$eq(functions.col("magnitud_HCT")))),
								0).otherwise(1))
				.filter(functions.col("test").equalTo(0));

		return unionFiltrado(medicionesEntrantes, medicionesErroneasNONO2);
	}

	public static Dataset<Row> recalculoHCT(Dataset<Row> medicionesFiltradas) {
		// Se realiza el recálculo de NOx
		Dataset<Row> medicionesRecalculoNOx = medicionesFiltradas
				.filter(medicionesFiltradas.col("magnitud").isin(magnitudHCT, magnitudCH4)).groupBy("estacion", "fecha")
				.agg(functions.max(functions
						.when(functions.col("magnitud").equalTo(magnitudHCT), functions.col("valor")).otherwise(null))
						.as("valor_HCT"),
						functions.max(
								functions.when(functions.col("magnitud").equalTo(magnitudCH4), functions.col("valor"))
										.otherwise(null))
								.as("valor_CH4"))
				.filter(functions.col("valor_HCT").isNotNull().and(functions.col("valor_CH4").isNotNull()))
				.withColumn("valor_recalculado", functions.col("valor_HCT").$minus(functions.col("valor_CH4")))
				.withColumnRenamed("fecha", "fecha_recalculo").withColumnRenamed("estacion", "estacion_recalculo");
		;
		return unionRecalculo(medicionesFiltradas, medicionesRecalculoNOx);
	}

	public static Dataset<Row> unionFiltrado(Dataset<Row> medicionesEntrantes, Dataset<Row> medicionesErroneasNONO2) {

		// Realiza una subconsulta para filtrar las medicionesEntrantes que no aparecen
		// en el groupBy
		Dataset<Row> medicionesFiltradasNONO2 = medicionesEntrantes
				.join(medicionesErroneasNONO2,
						medicionesEntrantes.col("estacion").equalTo(medicionesErroneasNONO2.col("estacion"))
								.and(medicionesEntrantes.col("fecha").equalTo(medicionesErroneasNONO2.col("fecha"))),
						"left_anti");

		return medicionesFiltradasNONO2;
	}

	public static Dataset<Row> unionRecalculo(Dataset<Row> medicionesFiltradas, Dataset<Row> medicionesRecalculo) {

		// Se actualizan todas las filas que contengan NOx
		Dataset<Row> medicionesRecalculadas = medicionesFiltradas
				.join(medicionesRecalculo,
						medicionesFiltradas.col("estacion").equalTo(medicionesRecalculo.col("estacion_recalculo"))
								.and(medicionesFiltradas.col("fecha")
										.equalTo(medicionesRecalculo.col("fecha_recalculo"))),
						"left")
				.withColumn("valor",
						functions
								.when(medicionesFiltradas.col("magnitud").equalTo(9),
										medicionesRecalculo.col("valor_recalculado"))
								.otherwise(medicionesFiltradas.col("valor")))
				.select("equipo", "estacion", "estado", "fecha", "indice", "intervalo", "magnitud", "nivel", "valor");

		return medicionesRecalculadas;

	}

}
