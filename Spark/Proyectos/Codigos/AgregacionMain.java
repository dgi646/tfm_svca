package es.agregacion;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.sivca.dominio.Medicion;

public class Main {
	private static final String APP_NAME = "Agregacion";

	public static void main(String[] args) throws StreamingQueryException, InterruptedException, SQLException {

		// Configuración de Spark
		SparkConf conf = new SparkConf().set("spark.sql.adaptive.enabled", "true")
				.set("spark.metrics.namespace", "Agregacion").set("spark.sql.streaming.metricsEnabled", "true")
				.set("spark.sql.adaptive.skewJoin.enabled", "true").setAppName(APP_NAME);

		JavaStreamingContext ssc = new JavaStreamingContext(conf, new Duration(1000));
		SparkSession spark = SparkSession.builder().config(conf).getOrCreate();// Initialize KafkaConsumer
		spark.sparkContext().setLogLevel("ERROR");
		// Parametros del servidor
		Map<String, Object> kafkaParams = new HashMap<String, Object>();
		String kafkaServer = "localhost:9092";
		kafkaParams.put("bootstrap.servers", kafkaServer);
		kafkaParams.put("key.deserializer", StringDeserializer.class);
		kafkaParams.put("value.deserializer", StringDeserializer.class);
		kafkaParams.put("group.id", "1");
		kafkaParams.put("auto.offset.reset", "latest");
		kafkaParams.put("enable.auto.commit", false);

		// Parametros deserializador
		Map<String, Object> kafkaParamsOut = new HashMap<String, Object>();
		String kafkaServerOut = "localhost:9092";
		kafkaParamsOut.put("bootstrap.servers", kafkaServerOut);
		kafkaParamsOut.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		kafkaParamsOut.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

		String topic = "agregacion_topic";
		Collection<String> topics = Arrays.asList(topic);

		// Stream que recibe los datos en crudo
		JavaInputDStream<ConsumerRecord<String, String>> stream = KafkaUtils.createDirectStream(ssc,
				LocationStrategies.PreferConsistent(),
				ConsumerStrategies.<String, String>Subscribe(topics, kafkaParams));

		// Se transforman a la clase medición
		JavaDStream<Medicion> userEvents = stream.map(record -> {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(record.value(), Medicion.class);
		});
		JavaDStream<Medicion> userEventsWindowed = userEvents.window(Durations.seconds(10), Durations.seconds(1));

		// Se convierte en Dataset y se hace una transformación simple
		userEventsWindowed.foreachRDD(rdd -> {
			if (!rdd.isEmpty()) {

				Dataset<Row> medicionesEntrantes = spark.createDataset(rdd.rdd(), Encoders.bean(Medicion.class)).toDF();
				// Agrupar y agregar los datos cada minuto

				Dataset<Row> aggregatedData = medicionesEntrantes
						.groupBy(functions.window(functions.col("fecha"), "60 seconds"), functions.col("estacion"),
								functions.col("estacion"), functions.col("magnitud"), functions.col("equipo"),
								functions.col("nivel"), functions.col("intervalo"))
						.agg(functions.round(functions.avg("valor"), 2).alias("valor"),
								functions.count("*").alias("recuento"))
						.filter(functions.col("recuento").$eq$eq$eq(60)).select(functions.col("recuento"),
								functions.col("equipo").as("equipo_id"), functions.col("estacion").as("estacion_id"),
								functions.col("window").getField("start").as("fecha"),
								functions.col("magnitud").as("magnitud_id"), functions.col("valor"),
								functions.col("nivel"), functions.col("intervalo").as("intervalo_integracion"));

				aggregatedData = aggregatedData.withColumn("valor", functions.round(aggregatedData.col("valor"), 3))
						.withColumn("med_estado_id", functions.lit(1));

				aggregatedData = aggregatedData.drop("recuento");
				medicionesEntrantes = medicionesEntrantes.selectExpr("estacion as estacion_id", "equipo as equipo_id",
						"magnitud as magnitud_id", "intervalo as intervalo_integracion", "nivel", "fecha", "valor",
						"estado as med_estado_id", "indice");

				insertaMedicionesSegundoBBDD(medicionesEntrantes);
				insertaBBDD(aggregatedData);

			}
		});

		ssc.start();
		ssc.awaitTermination();
	}

	private static void insertaMedicionesSegundoBBDD(Dataset<Row> ds) {
		try {
			// Cambiar el modo de guardado a Append
			ds.write().mode(SaveMode.Append).format("jdbc").option("driver", "org.postgresql.Driver")
					.option("url", "jdbc:postgresql://localhost:5432/postgres?currentSchema=public")
					.option("dbtable", "med_seg").option("user", "postgres").option("password", "postgres").save();
		} catch (Exception e) {
			System.out.println("Se ha producido un error SQL:" + e);
		}
	}

	private static void insertaBBDD(Dataset<Row> ds) {
		try {
			// Cambiar el modo de guardado a Append
			ds.write().mode(SaveMode.Append).format("jdbc").option("driver", "org.postgresql.Driver")
					.option("url", "jdbc:postgresql://localhost:5432/postgres?currentSchema=public")
					.option("dbtable", "med_min").option("user", "postgres").option("password", "postgres").save();
		} catch (Exception e) {
			System.out.println("Se ha producido un error SQL:" + e);
		}
	}
}
