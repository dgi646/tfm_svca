package es.deserializador.dominio;

import java.io.Serializable;

public class Medicion implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer fuente_datos;
	private Integer intervalo;
	private Integer estacion;
	private Integer magnitud;
	private String equipo;
	private Integer nivel;
	private String fecha;
	private Double valor;
	private String estado;
	private Integer indice;

	/**
	 * @return the intervalo
	 */
	public Integer getIntervalo() {

		return intervalo;
	}

	/**
	 * @param intervalo the intervalo to set
	 */
	public void setIntervalo(Integer intervalo) {

		this.intervalo = intervalo;
	}

	/**
	 * @return the estacion
	 */
	public Integer getEstacion() {

		return estacion;
	}

	/**
	 * @param estacion the estacion to set
	 */
	public void setEstacion(Integer estacion) {

		this.estacion = estacion;
	}

	/**
	 * @return the magnitud
	 */
	public Integer getMagnitud() {

		return magnitud;
	}

	/**
	 * @param magnitud the magnitud to set
	 */
	public void setMagnitud(Integer magnitud) {

		this.magnitud = magnitud;
	}

	/**
	 * @return the nivel
	 */
	public Integer getNivel() {

		return nivel;
	}

	/**
	 * @param nivel the nivel to set
	 */
	public void setNivel(Integer nivel) {

		this.nivel = nivel;
	}

	public String getFecha() {

		return fecha;
	}

	public void setFecha(String fecha) {

		this.fecha = fecha;
	}

	/**
	 * @return the valor
	 */
	public Double getValor() {

		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(Double valor) {

		this.valor = valor;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {

		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {

		this.estado = estado;
	}

	/**
	 * @return the indice
	 */
	public Integer getIndice() {

		return indice;
	}

	/**
	 * @param indice the indice to set
	 */
	public void setIndice(Integer indice) {

		this.indice = indice;
	}

	public String getEquipo() {
		return equipo;
	}

	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}

	public Integer getFuente_datos() {
		return fuente_datos;
	}

	public void setFuente_datos(Integer fuente_datos) {
		this.fuente_datos = fuente_datos;
	}

}
