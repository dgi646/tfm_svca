package es.deserializador.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.stereotype.Service;

import es.deserializador.dominio.Mediciones;

@Service
public class KafkaSender {

	// Annotation
	@Bean

	// Method
	public ProducerFactory<String, Mediciones> producerFactory() {

		// Creating a Map
		Map<String, Object> config = new HashMap<>();

		// Adding Configuration

		config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
//		config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "34.175.70.58:9092");

		config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

		return new DefaultKafkaProducerFactory<>(config);
	}

	// Annotation
	@Bean

	// Method
	public KafkaTemplate kafkaTemplate() {

		return new KafkaTemplate<>(producerFactory());
	}
}