package es.deserializador.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.deserializador.dominio.Medicion;
import es.deserializador.dominio.Mediciones;

@RestController
@RequestMapping(value = "/kafka-deserializador/")
public class ApacheKafkaWebController {

	// Autowiring Kafka Template
	@Autowired
	KafkaTemplate<String, Medicion> kafkaTemplate;

	private static final String TOPIC = "entrada_topic";

	// Annotation
	@PostMapping("/publish")
	public String publishMessage(@RequestBody Mediciones mediciones) {
		// Sending the message
		int contador = 0;
		for (Medicion medicion : mediciones.getMediciones()) {
			kafkaTemplate.send(TOPIC, medicion);
			contador += 1;
		}
		System.out.println("Mediciones enviadas:" + contador + " Hora:" + LocalDateTime.now());

		return "Published Successfully";
	}

}
