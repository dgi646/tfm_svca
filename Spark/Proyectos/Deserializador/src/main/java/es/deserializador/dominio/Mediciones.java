package es.deserializador.dominio;

import java.io.Serializable;
import java.util.List;

public class Mediciones implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<Medicion> mediciones;

	public List<Medicion> getMediciones() {
		return mediciones;
	}

	public void setMediciones(List<Medicion> mediciones) {
		this.mediciones = mediciones;
	}
}
