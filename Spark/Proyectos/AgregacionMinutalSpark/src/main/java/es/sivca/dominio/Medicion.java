package es.sivca.dominio;

import java.io.Serializable;
import java.sql.Timestamp;

public class Medicion implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer intervalo;
	private Integer estacion;
	private Integer magnitud;
	private Integer equipo;
	private Integer nivel;
	private Timestamp fecha;
	private Float valor;
	private Integer estado;
	private Integer indice;

	/**
	 * @return the intervalo
	 */
	public Integer getIntervalo() {

		return intervalo;
	}

	/**
	 * @param intervalo the intervalo to set
	 */
	public void setIntervalo(Integer intervalo) {

		this.intervalo = intervalo;
	}

	/**
	 * @return the estacion
	 */
	public Integer getEstacion() {

		return estacion;
	}

	/**
	 * @param estacion the estacion to set
	 */
	public void setEstacion(Integer estacion) {

		this.estacion = estacion;
	}

	/**
	 * @return the magnitud
	 */
	public Integer getMagnitud() {

		return magnitud;
	}

	/**
	 * @param magnitud the magnitud to set
	 */
	public void setMagnitud(Integer magnitud) {

		this.magnitud = magnitud;
	}

	/**
	 * @return the nivel
	 */
	public Integer getNivel() {

		return nivel;
	}

	/**
	 * @param nivel the nivel to set
	 */
	public void setNivel(Integer nivel) {

		this.nivel = nivel;
	}

	public Timestamp getFecha() {

		return fecha;
	}

	public void setFecha(Timestamp fecha) {

		this.fecha = fecha;
	}

	/**
	 * @return the valor
	 */
	public Float getValor() {

		return valor;
	}

	/**
	 * @param valor the valor to set
	 */
	public void setValor(Float valor) {

		this.valor = valor;
	}

	/**
	 * @return the estado
	 */
	public Integer getEstado() {

		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(Integer estado) {

		this.estado = estado;
	}

	/**
	 * @return the indice
	 */
	public Integer getIndice() {

		return indice;
	}

	/**
	 * @param indice the indice to set
	 */
	public void setIndice(Integer indice) {

		this.indice = indice;
	}

	public Integer getEquipo() {
		return equipo;
	}

	public void setEquipo(Integer equipo) {
		this.equipo = equipo;
	}

}
